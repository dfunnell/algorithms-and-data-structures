# - Selection sort works by splitting the array up into two sections. There is a sorted section and an unsorted section. It loops though selecting the smallest items in the unsorted section and swapping them to the correct index in the sorted section. Below is an indepth breakdown.
# - The algorithm works by utilizing 2 for loops.
# - The first for loop is used to determine the index the sorting algorithm is currently on. This index also separates the sorted section and the unsorted section of the list. Left of the index is sorted, right of the index is unsorted.
# - The first thing that occurs within this for loop is a variable minimum_index is set to the current index of the for loop. This minimum index is a place holder for the smallest value in the unsorted section.
# - The second nested for loop then performs a linear search of the unsorted section of the list to find the smallest value present.
# - This is done with an if statement that compares the current value at the index of the nested foor loop to the value at the current minimum_index.
# - If the value is smaller than the value at the current minimum_index, minimum_index is then updated to the new smallest value.
# - Once the nested for loop has made it way though the entire unsorted section of the list, a swap is performed to move the value at the minimum_index to the index of the first for loop.
# - This swap moves the current minimum value to the last value in the sorted section of the list.
# - This process continues until the first for loop has made its way though the entire list.
# - At this point, we know the list should be completely sorted.
# - The worst and average time complexity for this algorithm is O(n^2) because this algorithm contains a nested for loop that requires you to loop over each element in the list.


l = [4,23,3,42,6,57,66,8,75,43,24,435,7]

# def selection(arr):
#     n = len(arr)
#     for x in range(n):
#         minimum_index = x
#         for y in range(x + 1, n):
#             if arr[y] < arr[minimum_index]:
#                 minimum_index = y
#         arr[x], arr[minimum_index] = arr[minimum_index], arr[x]

#     return arr

# print(selection(l))




def selection_sort(sort_list):
    for x in range(len(sort_list)):
        min_index = x
        for y in range(x+1, len(sort_list)):
            if sort_list[y] < sort_list[min_index]:
                min_index = y
        sort_list[x], sort_list[min_index] = sort_list[min_index], sort_list[x]

    return sort_list

print(selection_sort(l))


l = [4,23,3,42,6,57,66,8,75,43,24,435,7]
