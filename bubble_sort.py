# This is Bubble Sort. You are looping though the list and swapping values if the value to the
# left is larger than the value to the right. This puts all higher values on the right
# bubbling them to the end of the list


l = [4, 23, 3, 42, 6, 57, 66, 8, 75, 43, 24, 435, 7]

# def bubble(arr):
#     n = len(arr)
#     for x in range(n):
#         for y in range(n-x-1):
#             if arr[y] > arr[y+1]:
#                 arr[y], arr[y+1] = arr[y+1], arr[y]

#     return arr

# print(bubble(l))


def bubble_sort(sort_list):
    for x in range(len(sort_list)-1):
        for y in range(len(sort_list)-1-x):
            if sort_list[y] > sort_list[y+1]:
                sort_list[y], sort_list[y+1] = sort_list[y+1], sort_list[y]

    return sort_list


print(bubble_sort(l))
