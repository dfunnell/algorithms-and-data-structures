values = [2, 3, 6, 6, 7, 23, 45, 54, 342]
val = 23

def binary_search(l, val):
    left = 0
    right = len(l)
    while left != right:
        middle = (left + right)//2
        if val == l[middle]:
            return True
        elif l[middle] < val:
            left = middle + 1
        else:
            right = middle -1

    return False

print(binary_search(values, val))
