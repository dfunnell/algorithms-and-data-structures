
l = [4,23,3,42,6,57,66,8,75,43,24,435,7]

# def insertion(arr):
#     n = len(arr)
#     for x in range(1,n):
#         key = arr[x]
#         y = x - 1
#         while y >= 0 and key < arr[y]:
#             arr[y+1] = arr[y]
#             y -= 1
#         arr[y+1] = key

#     return arr

# print(insertion(l))

def insertion(arr):
    n = len(arr)
    for x in range(1,n):
        key = arr[x]
        y = x - 1
        while y >= 0 and key < arr[y]:
            arr[y+1] = arr[y]
            y -= 1
        arr[y+1] = key

    return arr

print(insertion(l))
