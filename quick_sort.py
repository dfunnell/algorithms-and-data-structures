# Quicksort has 2 major functions, there is a split function and a partition function. The main goal of these functions are to find the correct index for the pivot value in the sorted list. Once all of the pivot indexs are found, the array will be sorted.

# The Quicksort Function
# - The purpose of this quicksort function is used to chop the array into two segments separated at the pivots index. The pivots index is a location in the array where we know the value is at the correct location for the sorted list.
# - The function starts by checking 2 base cases. It first checks to see if the variable left and right are equal to None. This would be the case for the first time this function is called. If the values are equal to none, left is set equal to 0 and right is set equal to the length of the array -1.
# - The second base case is used to check if there is anything left to sort. This checks if the left index is greater than the right index. If it is, you know that section of the list is fully sorted.
# - After the base cases are checked, the partition function is called to find the index of the pivot. (See pivot function below)
# - With the pivot index known, we can recursively call the quicksort function twice. Once for the left of the pivot and once for the right of the pivot.
# - For the recursive function left of the pivot, we pass in the array, the left index and we set the right index to the pivot index - 1.
# - For the recursive function right of the pivot, we pass in the array, we set the left index to the pivot index + 1 and we pass in the right index.
# - Finally at this point we can return the sorted array.

# The Partition Function
# - The purpose of this partition function is to swap all values less tan the pivot to the left of the pivot, then swap the pivot into its correct index in the list.
# - The partition function starts by picking a pivot. The pivot in this case will always be the last value in your current list.
# - After picking a pivot, a variable star is created. This variable star is used to keep track of the number of items smaller than the pivot in the current list. It is initiallized with the value of left - 1.
# - We loop though the list and if a value is smaller than the pivot, we add 1 to the value of star and perform a swap of current value and the value in the list at the index of star.
# - Once this for loop is completed, all values smaller than the pivot will be to the left of the pivot.
# - We then add 1 to star and perform a swap with the pivot and the list at index star. This places the pivot at the correct index in the list.
# - Finally, the partition function returns the value star. Which is an index in the list that we now know is located at the correct sorted location in the list.

# The worst case time complexity of quicksort is O(n^2). This occurs when unlucky pivot values are selected such as when the pivot is the smallest or largest item in the partition range. Ideally, you would want the pivot value to be the median value in the list, this is what gives the average time complexity of O(nlog(n)).


values = [3,7,45,2,342,23,6,54,6]

def partition(values, left, right):
    # get the pivot value (the last item)
    pivot = values[right]

    # star keeps track of the end of the "smaller than the pivot" list
    star = left - 1

    # move all of  the items that are less than pivot to the beginning of the list
    for i in range(left, right):
        if values[i] <= pivot:
            star += 1
            values[star], values[i] = values[i], values[star]

    # move the pivot value into the "middle" of the list
    star += 1
    values[star], values[right] = values[right], values[star]

    # return the position of the pivot value
    return star

def quicksort(values, left=None, right=None):
    # handle the default calling case where only values is provided
    if left is None and right is None:
        left = 0
        right = len(values) - 1

    # if there is nothing left to sort return
    if left >= right or left < 0:
        return

    # partition values and get the position (index) of the pivot item
    p = partition(values, left, right)

    # recursively sort the left half
    quicksort(values, left, p - 1)

    # recursively sort the right half
    quicksort(values, p + 1, right)

    return values


print(quicksort(values))
